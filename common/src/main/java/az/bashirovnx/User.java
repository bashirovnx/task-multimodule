package az.bashirovnx;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class User {

    @Id
    private Long id;
    private String displayName;
    private String email;
    private String password;

}
