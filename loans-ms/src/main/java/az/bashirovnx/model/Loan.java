package az.bashirovnx.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "loans")
public class Loan {

    @Id
    private Long id;
    private Double amount;

}
