package az.bashirovnx.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "cards")
public class Card {

    @Id
    private Long id;
    private Double balance;

}
